using System;


namespace Player
{
    class PlayerInfo
    // basic player information such as name and the like
    {
        public PlayerInfo(string charName, string pronns, bool creation)
        {
            this.created = creation;//decerns between loaded or created character :P
            this.name = charName;
            this.pronouns = pronns;
            
        }
        public bool created { set; get; }
        public string name { set; get; }
        public string pronouns { set; get; }
        public int tile { set; get; } // the number tile from the begining of the array
        public int xPos { set; get; } //actual coordinates :>
        public int yPos { set; get; }
        public string ListAllStr()
        {
            return name + " " + pronouns;
        }
        public void Load()//implement later
        {

        }
    }
    class PlayerInventory
    {
        public PlayerInventory(int max)
        {
            this.InventoryMax = max;
        }
        public int InventoryMax { set; get; }
        public Dictionary<string, int> ItemNamesAndIDs = new Dictionary<string, int>();
    }
}