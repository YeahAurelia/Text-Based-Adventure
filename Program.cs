﻿using System;
using Player;
using System.Drawing;
using System.Runtime.Versioning;
using Pastel;
using WorldStuff;
namespace TBAMain
{
    internal class Program
    {
        PlayerInfo thePlayer = new PlayerInfo("a", "a", false);//this might be a bit dumb ngl
        PlayerInventory playerInventory = new PlayerInventory(1);
        World theWorld = new World([0]);

        int sceneSizeX = 30; //sizes in tiles each tile is 12 characters 4x3
        int sceneSizeY = 48;
        static void Main(string[] args)
        {
            Console.Clear();
            Console.WriteLine("Text Based Adventure game".Pastel(Color.FromArgb(95, 237, 114)));
            Console.WriteLine("Not Yet Named lol!!!");
            Program p = new();
            p.PreGame();
        }
        void PreGame()//basic stuff yknow
        {
            bool preLoop = true;
            string? input = null;
            while (preLoop)
            {
                Console.WriteLine("(n)ew player, (l)oad player, or (e)xit");//new character or load character
                while (input == null)
                    input = Console.ReadLine();
                if (input.ToLower() is "n" or "new")
                {
                    CharacterCreation();
                    if (thePlayer.created == true)
                    {
                        TheGame("new");
                    }
                }
                else if (input.ToLower() is "l" or "load")
                {
                    CharacterLoader();//Not yet implemented maybe should load worlds
                    TheGame("load");
                }
                else if (input.ToLower() is "e" or "exit")
                {
                    preLoop = false;
                }
                else
                {
                    input = null;
                }

            }
        }
        void TheGame(string loadOrNew)
        {
            if (loadOrNew == "load")
            {
                Console.WriteLine("Loading isn't yet implemented :P");//such a troll :3
            }
            else if (loadOrNew == "new")
            {
                RenderScene();
            }
            Console.WriteLine();
        }
        void RenderScene() //
        {
            int horizontalLineCount = 0;
            Console.Clear();
            while (horizontalLineCount != sceneSizeY)
            {
                string lineOfTile = getLine();
                Console.WriteLine(lineOfTile);
                horizontalLineCount++;
            }
            Console.WriteLine("______________");
            Console.Write("Enter Command: ");
            Console.Write(theWorld.Size);
            Console.ReadKey();
        }
        string getLine()//DO THIS SHIT AT SOMEPOINT!!!!5
        {
            return "A";
        }
        void CharacterCreation()// defines player name and pronouns
        {
            string? input = null;
            string? characterName = null;
            string? pronouns;
            bool restart = true;
            while (restart == true)
            {
                Console.WriteLine("Welcome to character creation!".Pastel(Color.FromArgb(95, 237, 114)));
                Console.WriteLine("What is your characters name?");
                while (characterName == null)
                {
                    characterName = Console.ReadLine();
                }
                Console.WriteLine("What pronouns should your character use?");
                Console.WriteLine("(H)e/Him, (S)he/Her (T)hey/Them");
                while (input == null)
                {
                    input = Console.ReadLine();
                }
                if (input.ToLower() == "h" || input.ToLower().Contains("he/him")) // needs work pronouns need expanding
                {
                    pronouns = "He/Him/His";
                }
                else if (input.ToLower() == "s" || input.ToLower().Contains("she/her"))
                {
                    pronouns = "She/Her/Hers";
                }
                else if (input.ToLower() == "t" || input.ToLower().Contains("they/them"))
                {
                    pronouns = "They/Them/Theirs";
                }
                else //pronouns default to they/them :P might add custom pronouns at a later point
                {
                    pronouns = "They/Them/Theirs";
                }
                //idk what else to add maybe a class system if i can be bothered but anyway!!!
                thePlayer.pronouns = pronouns;
                thePlayer.name = characterName;
                playerInventory.InventoryMax = 15;
                Console.WriteLine(thePlayer.ListAllStr());
                Console.WriteLine("Is this correct?[Y/n]");
                input = null;
                while (input == null)
                {
                    input = Console.ReadLine();
                }
                if (input.ToLower() == "y")
                {
                    Console.WriteLine("Yay!".Pastel(Color.FromArgb(95, 237, 114)));
                    restart = false;
                    theWorld = new World(File.ReadAllBytes("Worlds/MapTest/map.bm"));
                    thePlayer.created = true;

                }
            }
        }
        void CharacterLoader()//implement this later when i implement saving characters!!!!
        {

        }
    }
}
