using System.Security.Permissions;
using Pastel;

namespace WorldStuff
{
    public class World
    {
        public World(byte[] worldFile)
        {
            if (worldFile.Length > 12)
            {
                UncompressedWorld = worldFile;//no more decompression :(
                Size = UncompressedWorld.Length;
                TileTypes = [];
            }
        }
        public Dictionary<string, int> TileTypes { get; }
        public int TotalTiles { set; get; } //used to get what tile type the player is in
        public byte[] UncompressedWorld { get; }
        public int Size { get; }
        private byte[] WorldDecompressor(byte[] compressed)//ill keep it around but it isn't used anymore
        {

            List<byte> mapTilesList = [];
            List<byte> amountOfTiles = [];
            int arrayIndex = 13;
            byte currentTile = 0;
            bool isFirstPos = true;
            for (int i = 0; i != 12; i++)//this gets the header and adds it to the uncompressed world
            {
                mapTilesList.Add(compressed[i]);
            }
            for (int i = 13; i < compressed.Length; i++)//starts at 13 to skip the header and the random byte
            {
                if (compressed[i] == 0)
                {
                    if (!isFirstPos)
                    {
                        int stupid = ByteListToInt(amountOfTiles);
                        for (int step = 1; step <= stupid; step++)
                        {
                            mapTilesList.Add(currentTile);
                        }
                        amountOfTiles.Clear();
                    }
                    else
                    {
                        isFirstPos = false;
                    }
                }
                else
                {
                    if (compressed[i - 1] == 0)
                    {
                        currentTile = compressed[i];
                    }
                    else
                    {
                        amountOfTiles.Add(compressed[i]);
                    }
                }
            }
            return mapTilesList.ToArray();
        }
        private int ByteListToInt(List<byte> splintInt)
        {
            List<string> copOut = [];
            foreach (byte number in splintInt)
            {
                copOut.Add(number.ToString("X"));
            }
            string preconversion = string.Join("", copOut);
            Console.WriteLine(preconversion);
            Console.WriteLine(Convert.ToInt32(preconversion, 16));
            //Console.Read();
            return Convert.ToInt32(preconversion, 16);
        }
        private int getTotalBytes()
        {
            return 1;
        }
    }
}